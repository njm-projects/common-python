import os
import re
import struct
import numpy as np


def read_idx(filename: str):
    with open(filename, 'rb') as f:
        (zero, data_type, dims) = struct.unpack('>HBB', f.read(4))
        shape = struct.unpack(f">{'I'*dims}", f.read(4*dims))
        return np.frombuffer(f.read(), dtype=np.uint8).reshape(shape)

def read_netpbm(file):
    """
        Only binary PGM (P5) and binary PPM are supported (P6)
        BitMap (P1 & P4) and ASCII formats (P1 & P2 & P3) are not supported

        Type                Kind        Magic number	Magic number	Extension	Colors
        ====                ====        ============    ============    =========   ======
        Portable BitMap     binary      P1  ASCII       P4	binary	    .pbm	    0–1 (white & black)
        Portable GrayMap    gray        P2  ASCII	    P5	binary	    .pgm	    0–255 (gray scale)
        Portable PixMap     rgb         P3  ASCII	    P6	binary	    .ppm	    0–255 (RGB)
        """
    assert os.path.isfile(file)
    with open(file, mode='rb') as f:
        dat = re.findall(b"^\w*[Pp]([2356])\s(\d+)\s(\d+)\s(\d+)\s([\w\W]+)", f.read().lstrip())[0]
        (kind, width, height, maximum, data) = (int(dat[0]), int(dat[1]), int(dat[2]), int(dat[3]), dat[4])
        assert kind in (5, 6)
        image = np.frombuffer(data, dtype=np.uint8)
        image = image.reshape((width, height, 3) if (kind in (3, 6)) else (width, height))
        return image

def read_lfw(folder, list_file):
    assert os.path.isdir(folder) and os.path.isfile(list_file)
    list_names = {line.split(".")[0].lower() for line in open(list_file, 'r').readlines()}
    file_names = [os.path.join(folder, file) for file in os.listdir(folder) if file.split('.')[0].lower() in list_names]
    return np.array([read_netpbm(file) for file in file_names], dtype=np.uint8)