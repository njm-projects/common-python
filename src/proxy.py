import json
import multiprocessing as mp
import random
from urllib import request
import io
import zipfile
import urllib
import bs4
import os
from os import path
import time
import requests


### Pre 2017 ###

# class ProxyScrape:
#
#     def __init__(self, folder="json/", fileName="proxies.json", force_scrape=False, days=10):
#         self.proxies = []
#
#         time_ms = int(round(time.time() * 1000))
#
#         if folder.endswith("/"):
#             folder = folder[:-1]
#         file = "{}/{}".format(folder, fileName)
#
#         if not os.path.exists(folder):
#             print("\tProxies save dir does not exist: {} Making dir!".format(folder))
#             os.makedirs(folder)
#
#         if not path.isfile(file) or force_scrape:
#             if force_scrape:
#                 print("Forced proxy scrape... Scraping proxies!")
#             else:
#                 print("Proxy file: {} not found... Scraping proxies!".format(file))
#             self.proxies = self._scrapeAndDump(file, time_ms)
#         else:
#             try:
#                 read_obj = json.load(open(file, "r"))
#                 proxies = read_obj["proxies"]
#                 ms_time_created = read_obj["created"]
#                 if time_ms - ms_time_created > 1000 * 60 * 60 * 24 * days:
#                     print("Proxy file is old... Scraping proxies!")
#                     self.proxies = self._scrapeAndDump(file, time_ms)
#                 else:
#                     self.proxies = proxies
#             except:
#                 print("Invalid proxy file... Scraping proxies!")
#                 self.proxies = self._scrapeAndDump(file, time_ms)
#                 return
#
#     def _scrapeAndDump(self, file, time_ms):
#         proxies = self._scrapeHideMyAssProxies()
#         proxies_json = json.dumps({"created": time_ms, "proxies": proxies}, indent=2)
#         proxies_file = open(file, "w")
#         proxies_file.write(proxies_json)
#         print("Saved: {} proxies to: {}".format(len(proxies), file))
#         return proxies
#
#     def randomProxy(self):
#         index = random.randint(0, len(self.proxies) - 1)
#         return self.proxies[index]
#
#     def _scrapeHideMyAssProxies(self, hma_url="http://proxylist.hidemyass.com/search-1756621"):
#         html = request.urlopen(hma_url).read()
#         print("Scraping: \"{}\" for proxies!".format(hma_url))
#         # html = open("src.html").read()
#
#         try:
#             import lxml
#         except:
#             raise RuntimeError("LXML Package not found... Install it!")
#
#         soup = bs4.BeautifulSoup(html, "lxml")
#         print(soup)
#         tbody = soup.find_all("tbody")[0]
#         # trows = tbody.find_all("tr")
#
#         proxies = []
#         for row in trows:
#             entries = row.find_all("td")
#             entry_ip = entries[1].contents[1]
#             port = str(entries[2].string).strip()
#             protocal = str(entries[6].string).strip().lower()
#
#             style = str(entry_ip.contents[1].string)[2:]
#             entry_data = entry_ip.contents[1:]
#             style_dict = {dat[0]: "display:inline" in dat[1] for dat in [s.split("{") for s in style.split(".")]}
#             false_list = [klass for klass in style_dict if style_dict[klass] == False]
#
#             ip = ""
#             for tag in entry_data:
#                 if ("bs4.element.NavigableString" in str(tag.__class__)):
#                     ip += str(tag).strip()
#                     continue
#                 elif ("bs4.element.Tag" in str(tag.__class__)):
#                     if (tag.string is None):
#                         continue
#                     if not "display:none" in str(tag):
#                         valid = True
#                         for key in false_list:
#                             if (key in str(tag)):
#                                 valid = False
#                                 break
#                         if valid:
#                             ip += str(tag.string).strip()
#
#             proxy = "{}://{}:{}".format(protocal, ip, port)
#             proxies.append({protocal: proxy})
#
#         print("Scraped Proxies: {}".format(proxies))
#         return proxies



class ProxyScrape:

    def __init__(self, folder="json/", fileName="proxies.json", force_scrape=False, days=10):
        self.proxies = []

        time_ms = int(round(time.time() * 1000))
        file = os.path.join(folder, fileName)

        if not os.path.exists(folder):
            os.makedirs(folder)

        if not path.isfile(file) or force_scrape:
            print("Updating proxies!".format(file))
            self.proxies = self._scrapeAndDump(file, time_ms)
        else:
            try:
                read_obj = json.load(open(file, "r"))
                proxies = read_obj["proxies"]
                ms_time_created = read_obj["created"]
                if time_ms - ms_time_created > 1000 * 60 * 60 * 24 * days:
                    print("Proxy file is old... Scraping proxies!")
                    self.proxies = self._scrapeAndDump(file, time_ms)
                else:
                    self.proxies = proxies
            except:
                print("Invalid proxy file... Scraping proxies!")
                self.proxies = self._scrapeAndDump(file, time_ms)
                return

    def _scrapeAndDump(self, file, time_ms):
        proxies = self._downloadMorphProxies()
        proxies_json = json.dumps({"created": time_ms, "proxies": proxies}, indent=2)
        proxies_file = open(file, "w")
        proxies_file.write(proxies_json)
        print("Saved: {} proxies to: {}".format(len(proxies), file))
        return proxies

    def randomProxy(self):
        index = random.randint(0, len(self.proxies) - 1)
        return self.proxies[index]

    def _downloadMorphProxies(self, morph_api_key="vvZhwW9dGzZUcnjAO4mZ", num=100):
        morph_api_url = "https://api.morph.io/mistergiri/hide_my_ass_proxy_list_ip/data.json"
        r = requests.get(morph_api_url, params={
            'key': morph_api_key,
            'query': f"select * from 'hidemyass' where anonimity='High +KA' and _type='HTTPS' limit {num}"
        })
        proxies = [{row['_type'] : f"{row['_type']}://{row['ipaddress']}:{row['port']}"} for row in r.json()]
        print(f"Downloaded: {len(proxies)} proxies!")
        return proxies



ATTEMPTS = 10
PROXIES = ProxyScrape()

def downloadThreadedProxies(url_file_tuples, threads=16):
    m = mp.Pool(processes=threads)
    m.starmap(_download, url_file_tuples)

def directDownloadUnzip(url, folder, fileName):
    if folder.endswith("/"):
        folder = folder[:-1]
    file = "{}/{}".format(folder, fileName)

    urllib.request.urlretrieve(url, "{}.zip".format(file))

    zip_ref = zipfile.ZipFile("{}.zip".format(file), 'r')
    zip_ref.extractall(folder)
    zip_ref.close()


def _download(url, file):
    for i in range(ATTEMPTS):
        try:
            proxy = PROXIES.randomProxy()

            proxy_handler = request.ProxyHandler(proxy)
            proxy_auth_handler = request.ProxyBasicAuthHandler()
            opener = request.build_opener(proxy_handler, proxy_auth_handler)
            open_url = opener.open(url)  # add timeout?
            read = open_url.read()

            f = io.FileIO(file, "w")
            f.write(read)

            string = "Downloaded (try={}): {} : {}".format(i, url, file)
            print(string)

            return
        except:
            continue

    string = "Failed (tries={}): {} : {}".format(ATTEMPTS, url, file)
    print(string)












