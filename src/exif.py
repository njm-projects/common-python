import PIL.Image
import PIL.ExifTags
import shutil
import re
import os

import datetime


def has_attribs(exif, keys, values):
    for key, value in zip(keys, values):
        if not key in exif:
            return False
        if not value is None:
            if not exif[key] == value:
                return False
    return True

def get_exif(file):
    img = PIL.Image.open(file)
    exif_data = img._getexif()
    try:
        return { PIL.ExifTags.TAGS[k]: v for k, v in exif_data.items() if k in PIL.ExifTags.TAGS }
    except:
        return {}

def walk(root, ext=None):
    exp = ".*\.(" + ext.lower() + ")$" if ext else ".*"
    found = []
    for path, subdirs, files in os.walk(root):
        for name in files:
            if not name.startswith("."):
                if re.match(exp, name.lower()):
                    found.append(os.path.join(path, name))
    return found

def exist(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)
    return dir

def group_images_by_date():
    root = "/Users/Nathan/Desktop/DCIM - Phone"
    valid_folder = exist(os.path.join(root, "camera"))
    invalid_folder = exist(os.path.join(root, "external"))

    for file in walk(valid_folder, "jpg"):
        exif = get_exif(file)

        if "DateTime" in exif:
            date = exif["DateTime"]
            print(file)
            print(date)
            date = datetime.datetime.strptime(date, '%Y:%m:%d %H:%M:%S')
            folder = date.strftime('%Y-%m')
            dir = exist(os.path.join(valid_folder, folder))
            print("{} : {}".format(file, folder))

            shutil.move(file, dir)