import numpy
import numpy as np
import matplotlib.pyplot as plt
from scipy import misc


def show_grid(images, title=None, show=True):
    assert (len(images.shape) == 3)
    dim = np.ceil(len(images) ** 0.5)
    fig = plt.figure() if title is None else plt.figure(num=title)
    for i, image in enumerate(images):
        fig.add_subplot(dim, dim, i+1)
        plt.axis('off')
        plt.imshow(image, cmap='gray')
    if show:
        plt.show()

def resize_all(images_array, size):
    assert (len(size) == 2) and (len(images_array.shape) == 3)
    if images_array.shape[1:] == size:
        print("Already Resized")
        return images_array
    temp = numpy.zeros((images_array.shape[0], *size), dtype=numpy.uint8)
    for i, image in enumerate(images_array):
        temp[i, :, :] = misc.imresize(image, size)
    return temp


def prep_images(images_array, size):
    assert (len(size) == 2) and  (len(images_array.shape) == 3)
    return resize_all(images_array, size).reshape((-1, size[0]*size[1])).astype('float32') / 255